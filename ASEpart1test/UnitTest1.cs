﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using ASEpart1;
using System.Collections.Generic;
using System.Text;
using System.Drawing;



namespace UnitTestProject1
{
    [TestClass]
    public class ASEpart1Tests
    {
        [TestMethod]

        public bool CompareBitmapsLazy(Bitmap OutputBitmap1, Bitmap OutputBitmap2)
        {
            if (OutputBitmap1 == null || OutputBitmap2 == null)
                return false;
            if (object.Equals(OutputBitmap1, OutputBitmap2))
                return true;
            if (!OutputBitmap1.Size.Equals(OutputBitmap2.Size) || !OutputBitmap1.PixelFormat.Equals(OutputBitmap2.PixelFormat))
                return false;


            for (int column = 0; column < OutputBitmap1.Width; column++)
            {
                for (int row = 0; row < OutputBitmap1.Height; row++)
                {
                    if (!OutputBitmap1.GetPixel(column, row).Equals(OutputBitmap2.GetPixel(column, row)))
                        return false;
                }
            }

            return true;
        }

        public void TestingDrawToMethod()
        {

            Bitmap OutputBitmap1 = new Bitmap(640, 480);
            Bitmap OutputBitmap2 = new Bitmap(640, 480);

            Graphics gp1 = Graphics.FromImage(OutputBitmap1);
            Graphics gp2 = Graphics.FromImage(OutputBitmap2);

            Canvas canvas = new Canvas(gp2);
            Commands p = new Commands(canvas);
            gp1.DrawLine(new Pen(Color.Black), new Point(0, 0), new Point(100, 100));

            p.runCmds("drawto 100,100");

            CompareBitmapsLazy(OutputBitmap1, OutputBitmap2);
        }



    }
}
