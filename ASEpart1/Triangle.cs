﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEpart1
{
    public class Triangle : Shape 
    {
       // protected int pnt1, pnt2, pnt3, pnt4, pnt5, pnt6;
        Point[] pnt = new Point[3];
        public Triangle (Color color, int pnt1, int pnt2, int pnt3, int pnt4, int pnt5, int pnt6) : 
            base (color, pnt1, pnt2, pnt3, pnt4, pnt5, pnt6)
        {

            pnt[0].X = pnt1;
            pnt[0].Y = pnt2;

            pnt[1].X = pnt3;
            pnt[1].Y = pnt4;

            pnt[2].X = pnt5;
            pnt[2].Y = pnt6;
        }

        public override void Paint (Graphics g)
        {
            g.DrawPolygon(Pen, pnt);

        }

        public override void FillPaint(Graphics g)
        {
            g.DrawPolygon(Pen, pnt);

        }


    }
}
