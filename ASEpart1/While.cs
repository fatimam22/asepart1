﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEpart1
{
    class While
    {
        VariableContainer varCont;
        int pointer;
        string statement;
        Program program;

        public While (VariableContainer varCont, int pointer, string statement, Program program)
        {
            this.varCont = varCont;
            this.pointer = pointer;
            this.statement = statement;
            this.program = program;

        }

        public bool check()
        {
            return program.CheckCondition(statement);
        }        

        public int getPnt
        {
            get => pointer;
        }

    }
}
