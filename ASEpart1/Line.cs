﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEpart1
{
    public class Line: Shape
    {
        protected int  toX, toY;

        public Line (Color color, int endX, int endY, int xPos, int yPos)
            : base(color, xPos, yPos)
        {
            toX = endX;
            toY = endY;
        }

        public override void Paint(Graphics g)
        {
            g.DrawLine(Pen, xPos, yPos, toX, toY);
        }

        public override void FillPaint(Graphics g)
        {
            g.DrawLine(Pen, xPos, yPos, toX, toY);
        }

    }
}
