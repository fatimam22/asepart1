﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEpart1
{
    class Method
    {
        int startPnt, endPnt;
        string[] param;
        
        public Method(int sPnt, int ePnt, string[] param )
        {
            startPnt = sPnt;
            endPnt = ePnt;
            this.param = param;

        }
        
        public VariableContainer MethodCont(int[] expression)
        {
            if (expression.Length != param.Length)
                throw new ArgumentException("Invalid paramaters");

            VariableContainer variable = new VariableContainer();
            for (int i = 0; i < param.Length; i++)
            {
                variable.ChangeVar(param[i], expression[i]);
            }

            return variable;

        }

        public int startPoint
        {
            get => startPnt;
        }
        
        public int endPoint
        {
            get => endPnt;
        }
 
    }

}
