﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static ASEpart1.Shape;

namespace ASEpart1
{
    public partial class Form1 : Form
    {
        
        
        Bitmap OutputBitmap = new Bitmap(640, 480);
        Canvas Mycanvas;
        Commands commands;
        Line line;
        Circle circle;
        Triangle triangle;
        Drawing Draw;
        
        //Sets up picture box as a cavas that will display drawings from cammands
        public Form1()
        {
            InitializeComponent();
            //Mycanvas = new Canvas(Graphics.FromImage(OutputBitmap));
            Draw = new Drawing(pictureBox1);
            
            pictureBox1.BackColor = Color.White;
            commands = new Commands(Mycanvas, Draw);
            pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(Draw.pictureBox1_Paint);
           
                //commands = new Commands(Draw);    
        }

        //takes user inputs from command line and runs when enter key is pressed
     
        public void commandLine_KeyDown_1(object sender, KeyEventArgs e)
        {
            
            Console.WriteLine("key down");
            String Command = commandLine.Text.Trim().ToLower();
      
            if (e.KeyCode == Keys.Enter)
            {
               //commands.runLine(commandLine.Text);
               
                if (commandLine.Text.ToLower() == "run")
                {
                    commands.runScript(richTextBox1.Text);
                    commandLine.Text = "";
                    Draw.update();

                    Refresh();
                    //pictureBox1.Invalidate();
                }
                else
                {
                    commands.callRunLine(richTextBox1.Text, commandLine.Text);
                    if(!commandLine.Text.Equals("run"))
                    commands.runLine(commandLine.Text);
                }

                pictureBox1.Refresh();
                commandLine.Text = "";
            }
        }
   
     

        private void runCode (object sender, EventArgs e)
        {
            commands.runScript(commandLine.Text);
        }


        //private void pictureBox1_Paint(object sender, PaintEventArgs e)
        //{
        //    Graphics g = e.Graphics;
        //    Draw.pictureBox1_Paint();
        //    g.DrawImageUnscaled(OutputBitmap, 0, 0);
        //}
        




        //implements open function 
        private void OpenBtn_Click(object sender, EventArgs e)
        {

            Stream openS;
            OpenFileDialog Open = new OpenFileDialog();
            
            if (Open.ShowDialog() == DialogResult.OK)
            {
                if ((openS = Open.OpenFile()) != null)
                {
                    string file = Open.FileName;
                    String str = File.ReadAllText(file);
                    richTextBox1.Text = str;
                }

            }


        }

        private void run (object sender, EventArgs e)
        {
            if (richTextBox1.Text.ToLower() == "reset")
            {
                Draw.clear();
                commands = new Commands(Mycanvas,Draw);
                Draw.update();
            }
            else
            {
                commands.callRunLine(richTextBox1.Text, commandLine.Text);
            }
        }





        private void Save_Click(object sender, EventArgs e)
        {
          
            
            SaveFileDialog Save = new SaveFileDialog();

           if (Save.ShowDialog() == DialogResult.OK)
            {
                using (Stream saveS = File.Open(Save.FileName, FileMode.CreateNew))
                using (StreamWriter sw = new StreamWriter(saveS))
                {
                    sw.Write(richTextBox1.Text);
                }

            }

        }

        private void button3_Click(object sender, EventArgs e)
        {


            if (richTextBox1.Text.ToLower() == "run")
            {
                Draw.clear();
                commands = new Commands(Mycanvas, Draw);
                Draw.update();
            }
            else
            {
                commands.callRunLine(richTextBox1.Text, commandLine.Text);
            }
          //  commands.runCmds(commandLine.Text);

        }
    }
}