﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ASEpart1
{
    public class Canvas
    {
        Graphics g;
        protected Pen Pen;
        protected SolidBrush PaintBrush;
        protected int xPos, yPos;
        Point[] pnt = new Point[3];
          
        public Canvas(Graphics g)
        {
            this.g = g;
            xPos = yPos = 0;
            Pen = new Pen(Color.Black, 1);
            PaintBrush = new SolidBrush(Color.Red);
        }

        public Canvas(Canvas c)
        {
        }

        public void setPenColor(string pencolour)
        {
            Pen = new Pen(Color.FromName(pencolour));
         
        }
        public void FillShape(string fillColor)
        {
           
            PaintBrush = new SolidBrush(Color.FromName(fillColor));
        }
        public void MoveTo(int mtoX, int mtoY)
        {
            xPos = mtoX;
            yPos = mtoY;
        }

      
        public void DrawLine(int toX, int toY)
        {
            g.DrawLine(Pen, xPos, yPos, toX, toY);
           xPos = toX;
           yPos = toY;   
        }

        public void DrawRectangle(int width, int height)
        {
             g.DrawRectangle(Pen, xPos, yPos, xPos + width, yPos + height);
        }

        public void FillRectangle(int width, int height)
        {
            g.FillRectangle(PaintBrush, xPos, yPos, xPos + width, yPos + height);
        }
        public void DrawCircle(float radius)
        {
            g.DrawEllipse(Pen, xPos - radius, yPos - radius, 
                radius + radius, radius + radius);
        }
      
        public void DrawCircle(float cntrX, float cntrY, float radius)
        {
            g.DrawEllipse(Pen, cntrX - radius, cntrY - radius, 
                radius + radius, radius + radius);

        }
        public void FillCircle(float radius)
        {
            g.FillEllipse(PaintBrush, xPos - radius, yPos - radius,
                radius + radius, radius + radius);
        }
        public void DrawTriangle(int pnt1, int pnt2, int pnt3, int pnt4, int pnt5, int pnt6)
        {
            pnt[0].X = pnt1;
            pnt[0].Y = pnt2;

            pnt[1].X = pnt3;
            pnt[1].Y = pnt4;

            pnt[2].X = pnt5;
            pnt[2].Y = pnt6;
            g.DrawPolygon(Pen, pnt);

        }

        public void FillTriangle(int pnt1, int pnt2, int pnt3, int pnt4, int pnt5, int pnt6)
        {
            pnt[0].X = pnt1;
            pnt[0].Y = pnt2;

            pnt[1].X = pnt3;
            pnt[1].Y = pnt4;

            pnt[2].X = pnt5;
            pnt[2].Y = pnt6;
            g.FillPolygon(PaintBrush, pnt);

        }


        public void ClearCanvas()
        {
            g.Clear(Color.White);
            //  g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
        }

       
    }


}