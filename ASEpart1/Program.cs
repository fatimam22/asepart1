﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASEpart1
{
    class Program
    {
        VariableFactory varFactory;
   
        public Program(VariableFactory varFactory)
        {

            this.varFactory = varFactory;

        }

        protected string ChangeVariables(string replace)
        {
            foreach (VariableContainer variable in varFactory.ContainsVar)
            {
                foreach (KeyValuePair<string, int> value in variable.variables)
                {
                    if (replace.Contains(value.Key))
                    {
                        replace = replace.Replace(value.Key, value.Value.ToString());
                    }

                }
                    

            }

            return replace;
        
        }
        
        public int Read(string replace)
        {
            DataTable dataTable = new DataTable();
            replace = ChangeVariables(replace);

            return Convert.ToInt32(dataTable.Compute(replace, ""));

        }

        public bool CheckCondition(string condt)
        {
            DataTable dataTable = new DataTable();
            bool checkSymbol = false;
            foreach (string symbol in new string[] { "<", ">", "=", "!=", "<=", ">=" })
            {
                //
                if (condt.Contains(symbol))
                {
                    checkSymbol = true;
                    break;
                }
            }
            if (checkSymbol)
            {
                condt = ChangeVariables(condt);
                bool result = (bool)dataTable.Compute(condt, "");
                return result;
            }
            else
            {
                throw new Exception("No  valid boolean symbol ");
            }
        }

        public int CheckValue(string value)
        {
            if (int.TryParse(value, out int expression ))
            {
                return expression;
            }
            foreach (VariableContainer variable in varFactory.ContainsVar)
            {
                if (variable.variables.ContainsKey(value))
                {
                    return variable.variables[value];
                }
            }
            throw new Exception("Variable " + value + "is missing");
        }

        public bool TryCheckValue(string input, out int result)
        {
            try
            {
                result = CheckValue(input);
                return true;
            }
            catch (Exception e)
            {
                result = 0;
                return false;
            }
        }

        public byte CheckByte(string checkByt)
        {
            if (byte.TryParse(checkByt, out byte result))
            {
                return result;
            }
            foreach (VariableContainer variable in varFactory.ContainsVar)
            {
                var variables = variable.variables;
                if (variables.ContainsKey(checkByt))
                {
                    int value = variables[checkByt];
                    if (byte.MinValue <= value && value <= byte.MaxValue)
                    {
                        return (byte)value;
                    }
                    else
                    {
                        throw new Exception("Not within range");
                    }
                }
            }
            throw new Exception("Invalid expression: " + checkByt);
        }
        public bool TryCheckByte(string checkByt, out byte result)
        {
            try
            {
                result = CheckByte(checkByt);
                return true;
            }
            catch (Exception e)
            {
                result = 0;
                return false;
            }
        }


        public void ChangeVar (string ident, int num)
        {
            foreach (VariableContainer variable in varFactory.ContainsVar)
            {
                if (variable.CheckVar(ident))
                {
                    variable.ChangeVar(ident, num);
                    return;
                }
            }
           
            varFactory.container.ChangeVar(ident, num);
        }
    }
}

  

