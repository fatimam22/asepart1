﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEpart1
{
    public class Circle : Shape
    {

        protected int radius;
        protected int xPos, yPos;
        public Circle(Color color, int radius, int xPos, int yPos) : base (color, xPos, yPos)
        {
            this.radius = radius;

            this.xPos = xPos;
            this.yPos = yPos;
        }

        public override void Paint(Graphics g)
        {

            System.Drawing.Rectangle circle = new System.Drawing.Rectangle(xPos - radius, yPos - radius, radius + radius, radius + radius);
           
            g.DrawEllipse(Pen, circle);
        }

        public override void FillPaint(Graphics g)
        {

            g.DrawEllipse(Pen, xPos - radius, yPos - radius,
                radius + radius, radius + radius);
        }

    }
}
