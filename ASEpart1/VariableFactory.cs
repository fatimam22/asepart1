﻿using System;
using System.Collections.Generic;

namespace ASEpart1
{
    class VariableContainer
    {
        public Dictionary<string, int> variables;

        public VariableContainer()
        {
            variables = new Dictionary<string, int>();

        }

        public void ChangeVar(string ident, int num)
        {
            variables[ident] = num;
        }

        public bool CheckVar(string var)
        {
            if (variables.ContainsKey(var))
                return true;
            else
                return false;

        }


    }

    class VariableFactory
    {

        Stack<VariableContainer> content;
        Stack<While> whileLoop;
        Stack<IfStatement> ifStatement;
        Dictionary<string, Method> methods;
        Stack<int> points;
        Program program;

        public VariableFactory()
        {
            content = new Stack<VariableContainer>();
            whileLoop = new Stack<While>();
            ifStatement = new Stack<IfStatement>();
            methods = new Dictionary<string, Method>();
            points = new Stack<int>();
            content.Push(new VariableContainer());
            program = new Program(this);
        }


        public Stack<VariableContainer> ContainsVar
        {
            get => content;
        }

        public VariableContainer container
        {
            get
            {
                return content.Peek();
            }
        }

        public void AddVariable (VariableContainer variable)
        {
            content.Push(variable);
        }

        public void CreateWhile (int point, string statement)
        {
            foreach (While whileloop in whileLoop) 
            {
                if (whileloop.getPnt == point)
                    return;
            }

            VariableContainer variable = new VariableContainer();
            content.Push(variable);
            While wLoop = new While(variable, point, statement, program);
            whileLoop.Push(wLoop);

        }


        public void CreateIfStatemnt (int pointer, string statement)
        {
            foreach (IfStatement ifS in ifStatement)
            {

                if (ifS.getPnt == pointer)
                return;
            }

            IfStatement IfStatmnt = new IfStatement(pointer, statement, program);
            ifStatement.Push(IfStatmnt);
        }

        public void deleteWhile()
        {
            content.Pop();
            whileLoop.Pop();
        }

        public void deleteIf()
        {
            ifStatement.Pop();
        }
    

        public While returnWhile
        {
            get
            {
                return whileLoop.Peek();
            }
        }

        public IfStatement returnIf
        {
            get
            {
                return ifStatement.Peek();
            }
        }
    

        public void CreateMethod (string mthd, int startPnt, int endPnt, string[] param)
        {
            if (methods.ContainsKey(mthd))
            {
                throw new Exception("change method name");
            }
            Method method = new Method(startPnt, endPnt, param);
            methods.Add(mthd, method);
        }
        
      


        public int CallMethod(string mthd, int point, int[] expression)
        {
            
            if (!methods.ContainsKey(mthd))
                throw new Exception("Method does not exist");
            Method method = methods[mthd];
            content.Push(method.MethodCont(expression));
            points.Push(point);
            return method.startPoint;
        }

        public int EndMethod()
        {
            if (points.Count == 0)
                throw new Exception("Does not exist in current method");
            content.Pop();
            return points.Pop();
        }


    }

}

   


