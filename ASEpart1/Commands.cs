﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASEpart1
{
    public class Commands
    {

        Canvas Mycanvas;
        ShapeBuilder shapeBuilder;
        Drawing Draw;
        Program program;
        VariableFactory varFactory;
        VariableContainer varCont;

        Dictionary<string, (byte, byte, byte, byte)> colours = new Dictionary<string, (byte, byte, byte, byte)>();

        bool readLine;
        int MethodPointer;
        string mthd;
        string[] parameters;
        bool methodRun = false;
   
        int pointer;





        public Commands(Canvas c, Drawing draw)
        {
            this.Mycanvas = c;
            this.Draw = draw;
            varFactory = new VariableFactory();
            program = new Program(varFactory);
           
            readLine = true;
            shapeBuilder = new ShapeBuilder(program);
            //Draw = draw;
        }
      
     
        protected int ParseFill(string num)
        {
            if (int.TryParse(num, out int x))
            {
                return x;
            }
            else
            {
                MessageBox.Show("An invalid number was entered");
                throw new Exception("An invalid number was entered");
            }
        }
        protected bool Shapefill;
        




       protected string[] parseMethod (string parseMthd, out string mthd)
        {
            mthd = "";
            int currentPoint;
            for(currentPoint = 0; parseMthd[currentPoint] != '('; currentPoint++)
            {
                if (parseMthd[currentPoint] == ' ')
                    throw new Exception("Remove space");
                mthd = mthd + parseMthd[currentPoint];
            }

            string param = "";
            for(currentPoint++; parseMthd[currentPoint] != ')'; currentPoint++)
            {
                param = param + parseMthd[currentPoint];
            }

            if (parseMthd.Length > 0)
            {
                Stack<string> paramaters = new Stack<string>();
                string identifier = "";
                int i;
                bool space = true;

                for (i = 0; i < param.Length; i++)
                {
                    char currentChar = param[i];
                    if (currentChar == ',')
                    {
                        if (identifier.Length == 0)
                            throw new Exception("Name paramater");
                        paramaters.Push(identifier);
                        identifier = "";
                        space = true;
                    }

                    else if (currentChar == ' ' && space) { }
                    else if (currentChar == ' ' && !space)
                    {
                        throw new Exception("remove space");
                    }
                    else
                    {
                        if (space)
                            space = false;
                        identifier = identifier + currentChar;
                    }
                }
                paramaters.Push(identifier);
                if (i != param.Length)
                    throw new Exception("invaild parameters");
                return paramaters.ToArray();
            }
            else
            {
                return new string[] { };
            }


        }



        public void runLine (string input)
        {
            runLine(input, 0);
        }

        public void runLine(String input, int pointer)
        {

            string[] inputs = input.Split(' ');

            if (readLine)
            {
                switch (inputs[0].ToLower())
                {

                 
                    case "triangle":
                    case "rectangle":
                    case "circle":
                    case "drawto":
                        Draw.AddShape(shapeBuilder.parseShape(input, inputs));

                        break;


                    //case "drawto":
                    //    //  var (x, y) = shapeBuilder.ParsePoint(inputs[1]);

                    //    Draw.AddShape(shapeBuilder.ParseLine(inputs[1]));
                    //    break;
                    //case "square":
                    //    int length = shapeBuilder.ParseNum(inputs[1]);
                    //    Mycanvas.DrawRectangle(length, length);

                    //    if (Shapefill == true)
                    //    {
                    //        Mycanvas.FillRectangle(length, length);
                    //    }
                    //    break;

                    //case "rectangle":
                    //    //  var (w, h) = shapeBuilder.ParsePoint(inputs[1]);

                    //    Draw.AddShape(shapeBuilder.ParseRect(inputs[1]));

                    //    if (Shapefill == true)
                    //    {
                    //        // Draw.AddShape(shapeBuilder.DrawRectangle(w, h));
                    //    }
                    //    break;
                    //case "circle":
                    //    int r = shapeBuilder.ParseNum(inputs[1]);

                    //    Draw.AddShape(shapeBuilder.DrawEclipse(r));
                    //    if (Shapefill == true)
                    //    {
                    //        Mycanvas.FillCircle(r);
                    //    }


                    //    break;
                    //case "triangle":
                    //    Draw.AddShape(shapeBuilder.ParseTriPoints(inputs[1]));

                    //    if (Shapefill == true)
                    //    {
                    //        //Mycanvas.FillTriangle(pnt1, pnt2, pnt3, pnt4, pnt5, pnt6);
                    //    }
                    //    break;

                    case "moveto":
                        var (mtox, mtoy) = shapeBuilder.ParsePoint(inputs[1]);
                        Mycanvas.MoveTo(mtox, mtoy);
                        break;

                    case "pen":
                        Mycanvas.setPenColor(inputs[1]);
                        break;

                    case "fillColor":
                        Mycanvas.setPenColor(inputs[1]);
                        break;

                    case "fill":
                        if (int.Parse(inputs[1]) == 1)
                        {
                            Shapefill = true;
                        }
                        if (int.Parse(inputs[1]) == 0)
                        {
                            Shapefill = false;
                        }
                        break;

                    case "reset":
                        Mycanvas.MoveTo(0, 0);
                        break;

                    case "clear":
                        Mycanvas.ClearCanvas();
                        break;
                    case "while":
                        if (inputs.Length > 1)
                        {
                            string conditon = String.Join(" ", inputs, 1, inputs.Length - 1);
                            varFactory.CreateWhile(pointer, conditon);
                            if (!varFactory.returnWhile.check()) ;
                            {
                                readLine = false;
                            }

                        }

                        break;

                    case "endWhileLoop":
                        try
                        {
                            pointer = varFactory.returnWhile.getPnt - 1;
                        }
                        catch (Exception e) { }

                        break;

                    case "if":
                        // if statements must have a condition so need to have more than one word
                        if (inputs.Length > 1)
                        {
                            string condition = input.Substring(3);
                            varFactory.CreateIfStatemnt(pointer, condition);
                            if (!varFactory.returnIf.check())
                            {
                                readLine = false;
                            }
                        }
                        break;

                    case "endifStatement":
                        if (inputs.Length > 1)
                            throw new Exception("Remove operands");
                        break;

                    case "method":
                        string mthdName;
                        string[] param2 = parseMethod(input.Substring(7), out mthdName);

                        MethodPointer = pointer;
                        mthd = mthdName;
                        parameters = param2;
                        methodRun = true;
                        readLine = false;
                        break;

                    case "endmethod":

                        pointer = varFactory.EndMethod();
                        break;

                    default:
                        if (inputs.Length > 2 && inputs[1] == "=")
                        {
                            if (inputs.Length < 3)
                            {
                                throw new Exception();
                            }
                            string expression = input.Substring(input.IndexOf('=') + 1);
                            int value = program.Read(expression);
                            program.ChangeVar(inputs[0], value);
                        }

                        else
                        {
                            MessageBox.Show("Not a valid command word");
                            throw new Exception("Not a valid command word");
                        }

                        break;
                }
            }

            else
            {
                if (!methodRun)
                {
                    switch (inputs[0].ToLower())
                    {
                        case "endWhileLoop":
                            varFactory.deleteWhile();
                            readLine = true;
                            break;

                        case "endif":
                            varFactory.deleteIf();
                            readLine = true;
                            break;
                        case "endmethod":
                            throw new Exception();
                            
                        default:
                            break;
                    }
                }
               
                else if (inputs[0].ToLower() == "endmethod")
                {
                    methodRun = false;
                    varFactory.CreateMethod(mthd, MethodPointer, pointer , parameters);
                    readLine = true;
                }



            }




        }



        public void callRunLine(string input, string code)
        {
            if (input == "run")
            {
                runScript(code);
            }
            else
            {
                try
                {
                    runLine(input);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message, "Syntax Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            Draw.update();

        }

        public bool runScript(string code, bool Error = true)
        {
            string[] cmdArray = code.Split('\n');
            for (pointer = 0; pointer < cmdArray.Length; pointer++)
            {
                try
                {
                    runLine(cmdArray[pointer], pointer);
                }
                catch (Exception e)
                {
                    if (Error)
                        MessageBox.Show(
                            String.Format("On line {0}: {1}", pointer, e.Message),
                            "Syntax Error",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                    return false;
                }
            }
            Draw.update();
            return true;
        }




        public void runCmds(string inptCmd, string command)
        {
            
            String[] cmds = inptCmd.Split('\n');
            for (int i = 0; i < cmds.Length; i++)
            {
                runLine(cmds[i]);
            }
         

        }

    }
}
