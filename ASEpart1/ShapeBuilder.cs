﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ASEpart1
{
    class ShapeBuilder
    {
        protected int x, y;
       // protected int endx, endy;
        protected float r;
        protected Color color;
        protected float penWidth;
        protected int width, height;

        protected Program program;

        public ShapeBuilder(Program program)
        {

            this.program = program;
         //   Clear();
            color = Color.Black;
        }

        public Line DrawLine(int endX, int endY) 
        {
            Line line = new Line(color, endX, endY, x, y);
            x = endX;
            y = endY;
            return line;
        }

        public Circle DrawEclipse(int radius) 
        {
            Circle circle = new Circle(color, radius, x, y);
            r = radius;
           
            return circle;
        }

        public Rectangle DrawRectangle (int width, int height)
        {
            Rectangle rect = new Rectangle(color, width, height);

            return rect;
        }

        public Triangle DrawTriangle (int pnt1, int pnt2, int pnt3, int pnt4, int pnt5, int pnt6)
        {
            Triangle triangle = new Triangle(color, pnt1, pnt2, pnt3, pnt4, pnt5, pnt6);

            return triangle;
        }

        public void Clear(Graphics g)
        {
            g.Clear(Color.White);
        }

        public Shape parseShape (string input, string [] cmd)
        {
            switch (cmd[0].ToLower())
            {
                case "circle":
                    return ParseNum(input, cmd);
                    break;
                case "triangle":
                    return ParseTriPoints(input, cmd);
                    break;
                case "rectangle":
                    return ParseRect(input, cmd);
                    break;
                case "drawto":
                    return ParseLine(input, cmd);
                    break;
                default:
                    return null;
                    break;
            }

        }











        public (int, int) ParsePoint(string coord)
        {
            string[] parts = coord.Split(',');
            if (parts.Length == 2)
            {
                if (int.TryParse(parts[0], out int x) && int.TryParse(parts[1], out int y))
                {
                    
                    
                    
                    return (x, y);
                }
                else
                {
                    MessageBox.Show("One of the coordinates is not a number.");
                    throw new Exception("One of the coordinates is not a number.");

                }
            }
            else
            {
                MessageBox.Show("Invalid number of coordinates.");
                throw new Exception("Invalid number of coordinates");

            }
        }

        public Line ParseLine(string cmd, string[] input)
        {
            if (input.Length == 2)
            {
                var (endx, endy) = ParsePoint(input[1]);

                return new Line(color, endx, endy, x, y);

            }
            else
            {

                MessageBox.Show("Invalid number of coordinates.");
                throw new Exception("Invalid number of coordinates");
            }
          

        }



        public Rectangle ParseRect(string coord, string[] cmd)
        {
            string[] parts = coord.Split(',');
            if (parts.Length == 2)
            {
                if (int.TryParse(parts[0], out int width) && int.TryParse(parts[1], out int height))
                {
                    return new Rectangle(color, width, height);
                }
                else
                {
                    MessageBox.Show("One of the coordinates is not a number.");
                    throw new Exception("One of the coordinates is not a number.");

                }
            }
            else
            {
                MessageBox.Show("Invalid number of coordinates.");
                throw new Exception("Invalid number of coordinates");

            }
        }











        //protected Circle ParseNum(string cmd, string[] input)
        //{
        //    if (input.Length == 1)
        //    {
        //        //(int, int) coord = ParsePoint(input[1]);
        //        if(program.TryCheckValue(input[1], out int radius))
        //        {
        //            return new Circle (color, x, y, radius);
        //        }

        //        else
        //        {
        //            MessageBox.Show("An invalid number was entered");
        //            throw new Exception("An invalid number was entered");
        //        }
        //    }
        //}


        public Triangle ParseTriPoints(string cmd, string[] points)
        {
           
            if (points.Length == 6)
            {
                if (int.TryParse(points[0], out int pnt1) && int.TryParse(points[1], out int pnt2)
                    && int.TryParse(points[2], out int pnt3) && int.TryParse(points[3], out int pnt4)
                     && int.TryParse(points[4], out int pnt5) && int.TryParse(points[5], out int pnt6))
                {
                    return new Triangle(color, pnt1, pnt2, pnt3, pnt4, pnt5, pnt6);
                    //(pnt1, pnt2, pnt3, pnt4, pnt5, pnt6);
                }
                else
                {
                    MessageBox.Show("One of the coordinates is not a number.");
                    throw new Exception("One of the coordinates is not a number.");

                }
            }
            else
            {
                MessageBox.Show("Invalid number of coordinates.");
                throw new Exception("Invalid number of coordinates");

            }
        }


        public Circle ParseNum(string cmd, string[] input)
        {
            if(int.TryParse(cmd, out int radius))
            {
                return new Circle(color, radius, x, y);
            }

            else
            {

                MessageBox.Show("An invalid number was entered");
                throw new Exception("An invalid number was entered");
            }

        }




    }
}
