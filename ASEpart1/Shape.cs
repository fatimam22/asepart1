﻿using System;
using System.Drawing;
using System.Threading;
namespace ASEpart1
{
    public abstract class Shape
    {
        Graphics g;
        protected Pen Pen;
        protected SolidBrush PaintBrush;
        protected int xPos, yPos;
        protected bool fill;
        Point[] pnt = new Point[3];
        Color color;
       

        public Shape(Color colour, int xPos, int yPos)
        {
            this.xPos = xPos;
            this.yPos = yPos;
            color = colour;
            Pen = new Pen(colour, 1);
            PaintBrush = new SolidBrush(colour);
            Point[] pnt = new Point[3];
        }

        public Shape(Color colour, int xPos, int yPos, int pnt3, int pnt4, int pnt5, int pnt6) 
            : this(colour, xPos, yPos)
        {
        }

        public void setPenColor(string pencolour)
        {
            Pen = new Pen(Color.FromName(pencolour));

        }
        public void FillShape(string fillColor)
        {

            PaintBrush = new SolidBrush(Color.FromName(fillColor));
        }

        abstract public void Paint(Graphics g);

        abstract public void FillPaint(Graphics g);








        /*public abstract class Shape
        {
            protected Pen Pen;
            protected SolidBrush PaintBrush;
            protected Color color;

            protected Shape (Color colour, float penWidth)
            {
                color = colour;
                PaintBrush = new SolidBrush(colour);
                Pen = new Pen(colour, penWidth);

            }

            public abstract void Paint(Graphics g);







            public Color GetColor()
            {
                return Pen.Color;
            }

            public float GetpenWidth()
            {
                return Pen.Width;
            }

        }*/

    }
}