﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;



namespace ASEpart1
{

    public class Drawing
    {
        Graphics g;


        

        protected List<Shape> shapes;

        protected System.Windows.Forms.PictureBox pictureBox1;


        public Drawing(System.Windows.Forms.PictureBox pictureBox1)
        {
            this.pictureBox1 = pictureBox1;
           // this.g = g;
            shapes = new List<Shape>();

        }



        public void AddShape(Shape shape)
        {
            shapes.Add(shape);
        }

        public void pictureBox1_Paint(object sender, System.Windows.Forms.PaintEventArgs p)
        {


            System.Drawing.Graphics g = p.Graphics;
            foreach (Shape shape in shapes)
            {
                shape.Paint(g);
                shape.FillPaint(g);
            }
            //g.DrawImageUnscaled(OutputBitmap, 0, 0);
        }

        public void clear()
        {
            shapes.Clear();

        }

        public void update()
        {
           pictureBox1.Refresh();
        }


        public Bitmap OutputBitmap (int x, int y)
        {
            Bitmap bitmap = new Bitmap(x, y);
            Graphics g = Graphics.FromImage(bitmap);
           
            foreach (Shape shape in shapes)
            {
                shape.Paint(g);
                shape.FillPaint(g);

            }
            return bitmap;
        }

        

    
    
    
    
    
    
    
    }



}
