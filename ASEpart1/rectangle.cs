﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEpart1
{
    public class Rectangle : Shape
    {
        protected int width, height;

        public Rectangle (Color color, int width, int height) : base (color, width, height)
        {
            this.width = width;
            this.height = height;
        }

        public override void Paint(Graphics g)
        {
            System.Drawing.Rectangle rect = new System.Drawing.Rectangle(xPos, yPos,  width, height);

            g.DrawRectangle(Pen, rect);
        }

        public override void FillPaint(Graphics g)
        {
            g.FillRectangle(PaintBrush, xPos, yPos, xPos + width, yPos + height);
        }



    }
}
